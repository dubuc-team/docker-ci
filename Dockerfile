FROM python:3.8-alpine
RUN apk add rsync openssh-client bash make python3-dev libffi-dev build-base openssl-dev
RUN apk add git
RUN apk update
ADD requirements.txt /tmp
RUN pip install virtualenv
RUN pip install -r /tmp/requirements.txt
RUN ansible-galaxy collection install community.general
RUN ansible-galaxy collection install community.docker
RUN rm -rf /var/cache/apk/* /tmp/*
